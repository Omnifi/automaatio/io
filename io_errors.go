package io

import (
	"fmt"
)

// IOError is used to signal a failed IO / file operation.
type IOError struct {
	// Error message.
	message string
}

// Returns the error message for IO operations.
func (ioe *IOError) Error() string {
	return fmt.Sprintf("IO error: %v", ioe.message)
}

// Returns a `IOError` with a premade message for cases when
// temporary file operations were unsuccessful.
func TemporaryIOError(message string) *IOError {
	return &IOError{
		message: fmt.Sprintf("temp file operation error: %s", message),
	}
}

// Returns a `IOError` with a premade message for cases when
// checksums fail.
func ChecksumError(message string) *IOError {
	return &IOError{
		message: fmt.Sprintf("checksum error: %s", message),
	}
}
