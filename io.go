package io

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

// GetTempFile creates a `io.File` object in a temporary space to be written to.
//
// `prefix` is optional.
func GetTempFile(prefix string) (*os.File, error) {
	// Create our Temp File:  This will create a filename like /tmp/prefix-123456
	// We can use a pattern of "pre-*.txt" to get an extension like: /tmp/pre-123456.txt
	tempFile, err := os.CreateTemp(os.TempDir(), prefix)

	if err != nil {
		return nil, TemporaryIOError(fmt.Sprintf("cannot create temporary file (%s)", err.Error()))
	}

	return tempFile, err
}

// CreateTempFile creates a temporary file and returns the file name (as a complete path).
func CreateTempFile(data string) (string, error) {
	tempFile, err := GetTempFile("")

	if err != nil {
		return "", TemporaryIOError(fmt.Sprintf("cannot create temporary file: %s", err))
	}

	// Example writing to the file
	text := []byte(data)
	if _, err = tempFile.Write(text); err != nil {
		return "", TemporaryIOError(fmt.Sprintf("failed to write to temporary file, %s", err))
	}

	// Close the file
	if err := tempFile.Close(); err != nil {
		return "", TemporaryIOError(fmt.Sprintf("%s", err))
	}

	fileName := tempFile.Name()

	return fileName, err
}

// ChecksumSHA256 checks if a file matches the checksum provided using
// SHA256.
func ChecksumSHA256(path string, checksum string) (bool, error) {
	file, err := os.Open(path)

	if err != nil {
		return false, ChecksumError(fmt.Sprintf("failed to open file '%s' for checksum: %s", path, err.Error()))
	}

	defer file.Close()

	hash := sha256.New()

	if _, err := io.Copy(hash, file); err != nil {
		return false, ChecksumError(err.Error())
	}

	sum := hex.EncodeToString(hash.Sum(nil))

	validChecksum := sum == checksum

	return validChecksum, nil
}

// WriteBytesToFile writes bytes to a given path
func WriteBytesToFile(bytes []byte, path string) error {
	absolutePath, err := filepath.Abs(path)

	if err != nil {
		return err
	}

	absoluteDir, _ := filepath.Split(absolutePath)
	err = os.MkdirAll(absoluteDir, 0700)

	if err != nil {
		return err
	}

	err = os.WriteFile(absolutePath, bytes, 0600)

	if err != nil {
		return err
	}

	return err
}
