package compression

import (
	"fmt"
)

// CompressionError is used to signal a failed compression /
// decompression operation.
type CompressionError struct {
	// Error message.
	message string
}

// Returns the error message for compression operations.
func (ce *CompressionError) Error() string {
	return fmt.Sprintf("compression error: %v", ce.message)
}

// Returns a `PulumiError` with a premade message for cases when
// the Pulumi installation does not work.
//func PulumiInstallationFailed() *CompressionError {
//	return &CompressionError{
//		Err: errors.New("pulumi installation failed"),
//	}
//}
