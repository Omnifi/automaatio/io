package compression

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
)

// Extracts a GNU zipped (gz) tape archive (tar) from a GNU zip stream,
// to a designated target location.
//
// Returns a `CompressionError` if unsuccessful.
func ExtractTarGz(gzipStream io.Reader, target string) error {
	uncompressedStream, err := gzip.NewReader(gzipStream)

	if err != nil {
		return &CompressionError{"ExtractTarGz: NewReader failed"}
	}

	tarReader := tar.NewReader(uncompressedStream)

	for {
		header, err := tarReader.Next()

		if err == io.EOF {
			break
		}

		if err != nil {
			return &CompressionError{fmt.Sprintf("ExtractTarGz: Next() failed: %s", err.Error())}
		}

		switch header.Typeflag {
		case tar.TypeDir:
			fileTarget := filepath.Join(target, header.Name)

			if err := makePathWithFileMode(fileTarget, 0755); err != nil {
				return &CompressionError{fmt.Sprintf("make path failed: %s", err.Error())}
			}
		case tar.TypeReg:
			fileTarget := filepath.Join(target, header.Name)

			err := extractFileWithMode(fileTarget, tarReader, 0770)

			if err != nil {
				return &CompressionError{fmt.Sprintf("file extraction failed: %s", err.Error())}
			}

			setModeForExtract(fileTarget, fs.FileMode(header.Mode))
		default:
			return &CompressionError{fmt.Sprintf(
				"extraction for unknown type: %b in %s",
				header.Typeflag,
				header.Name)}
		}
	}

	return nil
}

// Creates a path with a given `FileMode`.
//
// If unsuccessful, it returns a `*PathError`.
func makePathWithFileMode(path string, fileMode fs.FileMode) error {
	if err := os.Mkdir(path, fileMode); err != nil {
		return err
	}

	return nil
}

// Extracts a file from a tape archive reacher (`*tar.Reader`) with a given `FileMode`.
//
// If unsuccessful it returns a `CompressionError`.
func extractFileWithMode(path string, reader *tar.Reader, fileMode fs.FileMode) error {
	err := os.MkdirAll(filepath.Dir(path), 0770)

	if err != nil {
		return &CompressionError{fmt.Sprintf("make all paths failed: %s", err.Error())}
	}

	outFile, err := os.Create(path)
	if err != nil {
		return &CompressionError{fmt.Sprintf("create file failed: %s", err.Error())}
	}

	defer outFile.Close()

	if _, err := io.Copy(outFile, reader); err != nil {
		return &CompressionError{fmt.Sprintf("ExtractTarGz: Copy() failed: %s", err.Error())}
	}

	return err
}

// Sets the `FileMode` for an extracted file.
func setModeForExtract(path string, mode fs.FileMode) error {
	os.Chmod(path, mode)

	_, err := os.Stat(path)

	if err != nil {
		return err
	}

	return err
}
